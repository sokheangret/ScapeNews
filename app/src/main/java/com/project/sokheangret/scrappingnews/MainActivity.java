package com.project.sokheangret.scrappingnews;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.project.sokheangret.scrappingnews.adapter.RFARecyclerViewAdapter;
import com.project.sokheangret.scrappingnews.model.RFANews;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private List<RFANews> rfaNewsList;
    private RFARecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        rfaNewsList = new ArrayList<>();
        adapter = new RFARecyclerViewAdapter(this,rfaNewsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);


        new ScrapFromRFA().execute();

    }


    class ScrapFromRFA extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Document document = Jsoup.connect("http://www.rfa.org/khmer/").get();

                Elements threeFeature = document.select("div.three_featured");
                for(Element element : threeFeature){
                    String link = element.select("div.featured_image a").attr("href");
                    String picURL = element.select("img").attr("src");
                    String title = element.getElementsByTag("h2").text();
                    rfaNewsList.add(new RFANews(title, link, picURL));
                }

                Elements elements = document.select("div.single_column_teaser");
                for (Element element : elements) {
                    String link = element.select("div.teaserimg a").attr("href");
                    String picURL = element.select("img").attr("src");
                    String title = element.select("h2").text();
                    rfaNewsList.add(new RFANews(title, link, picURL));
                }

                Log.e("finish", "finish");
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("error", e.getMessage());
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.notifyDataSetChanged();

        }
    }


}
