package com.project.sokheangret.scrappingnews.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.sokheangret.scrappingnews.R;
import com.project.sokheangret.scrappingnews.model.KhmerReadNews;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sokheangret on 12/12/17.
 */

public class KhmerReadRecyclerViewAdapter extends RecyclerView.Adapter<KhmerReadRecyclerViewAdapter.ViewHolder>{

    private List<KhmerReadNews> khmerReadNewsList;
    private Context context;

    public KhmerReadRecyclerViewAdapter(List<KhmerReadNews> khmerReadNewsList, Context context) {
        this.khmerReadNewsList = khmerReadNewsList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.khmerread_news_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso
                .with(context)
                .load(khmerReadNewsList.get(position).getPicUrl())
                .into(holder.ivPic);

        Log.e("Title from adapter",khmerReadNewsList.get(position).getTitle());
        holder.tvTitle.setText(khmerReadNewsList.get(position).getTitle());
        holder.tvParagraph.setText(khmerReadNewsList.get(position).getParagraph());
    }

    @Override
    public int getItemCount() {
        return khmerReadNewsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvParagraph, tvTitle;
        ImageView ivPic;
        public ViewHolder(View itemView) {
            super(itemView);
            tvParagraph = itemView.findViewById(R.id.tvParagrap);
            tvTitle = itemView.findViewById(R.id.tvKRTitle);
            ivPic = itemView.findViewById(R.id.ivKRPicture);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(khmerReadNewsList.get(getAdapterPosition()).getLink())));
                }
            });
        }
    }
}
