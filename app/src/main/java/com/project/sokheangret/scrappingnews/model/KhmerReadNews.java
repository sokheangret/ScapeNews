package com.project.sokheangret.scrappingnews.model;

/**
 * Created by sokheangret on 12/12/17.
 */

public class KhmerReadNews {
    private String title;
    private String picUrl;
    private String link;
    private String paragraph;

    public KhmerReadNews(String title, String picUrl, String link, String paragraph) {
        this.title = title;
        this.picUrl = picUrl;
        this.link = link;
        this.paragraph = paragraph;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }
}
