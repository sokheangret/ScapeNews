package com.project.sokheangret.scrappingnews.model;

/**
 * Created by sokheangret on 12/11/17.
 */

public class RFANews {
    private String title;
    private String link;
    private String picUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public RFANews(String title, String link, String picUrl) {
        this.title = title;
        this.link = link;
        this.picUrl = picUrl;
    }
}
