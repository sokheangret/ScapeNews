package com.project.sokheangret.scrappingnews.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.project.sokheangret.scrappingnews.R;
import com.project.sokheangret.scrappingnews.model.RFANews;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sokheangret on 12/11/17.
 */

public class RFARecyclerViewAdapter extends RecyclerView.Adapter<RFARecyclerViewAdapter.ViewHolder> {
    private Context context;
    private List<RFANews> rfaNewsList;

    public RFARecyclerViewAdapter(Context context, List<RFANews> rfaNewsList) {
        this.context = context;
        this.rfaNewsList = rfaNewsList;
    }

    @Override
    public RFARecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rfa_news_item,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RFARecyclerViewAdapter.ViewHolder holder, int position) {
        Picasso
                .with(context)
                .load(rfaNewsList.get(position).getPicUrl())
                .into(holder.ivPicture);

        holder.tvTitle.setText(rfaNewsList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return rfaNewsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivPicture;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvKRTitle);
            ivPicture = itemView.findViewById(R.id.ivPicture);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(rfaNewsList.get(getAdapterPosition()).getLink())));
                }
            });
        }

    }
}
