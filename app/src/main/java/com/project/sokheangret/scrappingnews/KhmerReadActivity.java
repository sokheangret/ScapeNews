package com.project.sokheangret.scrappingnews;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.project.sokheangret.scrappingnews.adapter.KhmerReadRecyclerViewAdapter;
import com.project.sokheangret.scrappingnews.model.KhmerReadNews;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KhmerReadActivity extends AppCompatActivity {

    @BindView(R.id.rvKhmerReadNews)
    RecyclerView rvKhmerReadNews;
    private List<KhmerReadNews> khmerReadNewsList;
    private KhmerReadRecyclerViewAdapter khmerReadRecyclerViewAdapter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khmer_read);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        khmerReadNewsList = new ArrayList<>();
        khmerReadRecyclerViewAdapter =
                new KhmerReadRecyclerViewAdapter(khmerReadNewsList,
                        this);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this,
                        LinearLayoutManager.VERTICAL,
                        false);

        rvKhmerReadNews.setLayoutManager(linearLayoutManager);
        rvKhmerReadNews.setAdapter(khmerReadRecyclerViewAdapter);

        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        new ScrapOnKhmerRead().execute();

    }

    class ScrapOnKhmerRead extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String baseUrl = "http://khmeread.com/";
                Document document = Jsoup.connect(baseUrl+"categories/society").get();
                Elements elements = document.select("div.post-item");

                for (Element element : elements){
                    String url = element.select("div.boxify-post a").attr("href");
                    String picUrl = element.select("div.boxify-post a div.image-wrapper img")
                                    .attr("src");
                    String title = element.select("div.post-info a").text();
                    String paragraph = element.select("div.post-info div.post-short-desc").text();

                    Log.e("data",title );
                    khmerReadNewsList.add(
                            new KhmerReadNews(title,baseUrl+picUrl,baseUrl+url,paragraph)
                    );

                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("error catch",e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            khmerReadRecyclerViewAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }
    }


}
